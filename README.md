Peetriz JokeBundle

----
Step 1:
---- 
fill the required parameter 'joke_category' with something (for instance: joke_category: nerdy)

----
Step 2:
----
install the package: composer require peetriz/jokebundle

----
Step 3: 
----
Enable the bundle in the app/AppKernel.php registerBundles method:
new Peetriz\JokeBundle\PeetrizJokeBundle()

----
Step 4:
----
Register a new route in app/config/routing.yml:


peetriz_joke_api_get_jokes:
    path:     /api/joke/{num}
    defaults: { _controller: PeetrizJokeBundle:Default:getJokes, num: 1 }
    methods:  [GET]
    requirements:
        num: '\d+'
        
----

You can call the api with:

/api/joke/{num}

where the {num} is an optional param, the number of given jokes.