<?php

namespace Peetriz\JokeBundle\Service;

use Exception;

/**
 * Class CurlService
 * @package Peetriz\JokeBundle\Service
 */
class CurlService
{
    /**
     * @var string|array $jokeCategory
     */
    private $jokeCategory;

    /** @var ExcelService $excelService */
    private $excelService;

    /**
     * CurlService constructor.
     * @param $jokeCategory
     * @param ExcelService $excelService
     */
    public function __construct($jokeCategory, ExcelService $excelService)
    {
        $this->jokeCategory = $jokeCategory;
        $this->excelService = $excelService;
    }

    /**
     * @param $num
     * @return bool|string
     * @throws Exception
     */
    public function call($num)
    {
        $ch = curl_init();

        if ($ch === false) {
            throw new Exception('Failed to initalize curl.');
        }

        $apiUrl = "http://api.icndb.com/jokes/random/{$num}?limitTo=[{$this->jokeCategory}]";

        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        $response = json_decode($response);

        if ($response === false) {
            throw new Exception(curl_error($ch), curl_errno($ch));
        }

        curl_close($ch);

        if ($response->type == 'NoSuchCategoryException') {
            $response = 'No such category :( try something else, for instance: nerdy.';
        }

        return $this->excelService->writeXls($response->value);
    }
}
