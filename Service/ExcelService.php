<?php

namespace Peetriz\JokeBundle\Service;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ExcelService
 * @package Peetriz\JokeBundle\Service
 */
class ExcelService
{
    /**
     * @var string $rootDir
     */
    private $rootDir;

    /**
     * @var RequestStack $requestStack
     */
    private $requestStack;

    /**
     * @var Request $request
     */
    private $request;

    /**
     * ExcelService constructor.
     * @param $rootDir
     * @param RequestStack $requestStack
     */
    public function __construct($rootDir, RequestStack $requestStack)
    {
        $this->rootDir = $rootDir;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    /**
     * @param $jokes
     * @return array
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function writeXls($jokes)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Joke Id');
        $sheet->setCellValue('B1', 'Joke');

        $i = 2;
        foreach ($jokes as $joke) {

            $sheet->setCellValue("A{$i}", $joke->id);
            $sheet->setCellValue("B{$i}", $joke->joke);
            $i++;
        }

        $writer = new Xlsx($spreadsheet);
        $fileName = 'jokes_' . uniqid() . '.xlsx';
        $writer->save($this->rootDir . '/web/' . $fileName);

        $xlsPath = $this->request->getSchemeAndHttpHost() . $this->request->getBasePath() . '/' . $fileName;

        return ['Download link' => $xlsPath];
    }
}
