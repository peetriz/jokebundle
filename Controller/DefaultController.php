<?php

namespace Peetriz\JokeBundle\Controller;


use Peetriz\JokeBundle\Service\CurlService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DefaultController
 * @package Peetriz\JokeBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @param CurlService $curlService
     * @param $num
     * @return JsonResponse
     * @throws \Exception
     */
    public function getJokesAction(CurlService $curlService, $num)
    {
        $jokeUrl = $curlService->call($num);

        return new JsonResponse($jokeUrl);
    }
}
